1.Membuat Database 
-----------------------
CREATE DATABASE myshop;

2. Membuat tabel `categories`
---------------------------------------------------
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

Membuat tabel `items`
-------------------------------------------------------------------------------------
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `items_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

Membuat table `users`
----------------------------------------------------
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

3.Memasukan data kedalam tabel  `categories`
----------------------------------------------------
insert  into `categories`(`nama`) values 
('gadged'),
('cloth'),
('men'),
('women'),
('brended');

Memasukan data kedalam tabel `items`
------------------------------------------------------------------------------------
insert  into `items`(`nama`,`description`,`price`,`stock`,`category_id`) values 
('Sumsang b50','Hape keren dari merek sumsang',4000000,100,1),
('Uniklooh','Baju keren dari brand terutama',500000,50,2),
('IMHO Watch','Jam tangan anak yang jujur banget',2000000,10,1);

Memasukan data kedalam tabel `users`
------------------------------------------------------
insert  into `users`(`nama`,`email`,`password`) values 
('John Doe','john@doe.com','john123'),
('Jane Doe','jane@doe.com','jenita123');



4. Menampilkan data pada tabel user
----------------------------------
SELECT id,nama,email FROM users;

Menampilkan data pada tabel item 
------------------------------------
SELECT * FROM items WHERE price >1000000; 
SELECT * FROM items WHERE nama LIKE '%uniklo%';

Menampilkan data join dalam tabel `items` dan table `categories`
-------------------------------------------------------------------------------------------
SELECT items.`nama`,description,price,stock,category_id,categories.`nama` AS kategori FROM items LEFT JOIN categories ON category_id = categories.`id`;


5. Memperbarui data dalam tabel `items`
---------------------------------
UPDATE items SET price = 2500000
WHERE nama = 'Sumsang b50';
